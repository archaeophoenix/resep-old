<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  include_once('includes/header_start.php');
?>

    <!-- form Uploads -->
    <link href="<?php echo base_url(); ?>assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<?php include_once('includes/header_end.php'); ?>

    <div class="wrapper">
      <div class="container">
        <!-- Page-Title -->
        <div class="row">
          <div class="col-sm-12">
            <div class="page-title-box">
              <div class="btn-group pull-right">
                <ol class="breadcrumb hide-phone p-0 m-0">
                  <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $data[0]['title']; ?></a></li>
                  <li class="breadcrumb-item active">Detail Pengguna</li>
                </ol>
              </div>
              <h4 class="page-title">Detail Pengguna</h4>
            </div>
          </div>
        </div><!-- end page title end breadcrumb -->
      </div> <!-- end container -->
    </div><!-- end wrapper -->
    <!-- ==================
        PAGE CONTENT START
    ================== -->
    <div class="page-content-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-6">
            <div class="card m-b-20">
              <div class="card-block">
                <label><h6>Informasi Profil</h6></label>
                <form method="post" action="<?php echo base_url('user_operation/confirm'); ?>">
                  <div class="row table-responsive">
                    <table class="table table-bordered">
                      <tr>
                        <th width="30%">Title</th>
                        <td width="70%"><?php echo $info['title'] ?></td>
                      </tr>
                      <tr>
                        <th width="30%">Nama</th>
                        <td width="70%"><?php echo $info['doctor_name'] ?></td>
                      </tr>
                      <tr>
                        <th width="30%">Alamat Email</th>
                        <td width="70%"><?php echo $info['email'] ?></td>
                      </tr>
                      <tr>
                        <th width="30%">Nomor Telepon Kantor</th>
                        <td width="70%"><?php echo $info['mobile'] ?></td>
                      </tr>
                      <tr>
                        <th width="30%">Konfirmasi</th>
                        <td width="70%">
                          <input type="hidden" name="user_id" value="<?php echo $info['user_id']; ?>">
                          <input type="hidden" name="user_name" value="<?php echo $info['user_name']; ?>">
                          <select class="select" id="myselect2" name="status" required="">
                          <?php foreach ($stat as $key => $val) { ?>
                            <option value="<?php echo $key; ?>" <?php echo ($info['status'] == $key) ? 'selected="selected"' : ''; ?>><?php echo $val; ?></option>
                          <?php } ?>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" class="text-right">
                          <button title="Post" class="btn btn-1d btn-sm btn-outline-primary waves-effect waves-light"><i class="mdi mdi-account-check"></i></button>
                        </td>
                      </tr>
                    </table>
                  </div>
                </form>
              </div>
            </div>
          </div> <!-- end col -->
          <div class="col-6">
            <div class="card m-b-20">
              <div class="card-block">
                <label><h6>Informasi Profesi</h6></label>
                <div class="row table-responsive">
                  <table class="table table-bordered">
                    <tr>
                      <th width="30%">Spesialisasi</th>
                      <td width="70%"><?php echo $info['specialist'] ?></td>
                    </tr>
                    <tr>
                      <th width="30%">SIP</th>
                      <td width="70%"><?php echo $info['sip'] ?></td>
                    </tr>
                    <?php $office = json_decode($info['office'], true); ?>
                    <?php if(!empty($office)){ foreach ($office as $key => $val){ ?>
                    <tr>
                      <th width="30%">Alamat Praktek</th>
                      <td width="70%">
                        <?php echo $val['address']; ?><br>
                        <?php echo $val['day']; ?><br>
                        <?php echo $val['hour']; ?>
                      </td>
                    </tr>
                    <?php } } ?>
                  </table>
                </div>
              </div>
            </div>
          </div> <!-- end col -->
        </div> <!-- end row -->
        <div class="row">
          <div class="col-12">
            <div class="card m-b-20">
              <div class="card-block">
                <label><h6>Laporan Pengguna</h6></label>
                <div class="row table-responsive">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">Total Appointment</th>
                        <th class="text-center">Total Pasien</th>
                        <th class="text-center">Total Resep</th>
                        <th class="text-center">Total Faktur</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="text-right"><?php echo $appo['total']; ?></td>
                        <td class="text-right"><?php echo $pati['total']; ?></td>
                        <td class="text-right"><?php echo $pres['total']; ?></td>
                        <td class="text-right">Rp <?php echo number_format($invo,0,'','.'); ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div> <!-- end container -->
    </div><!-- end page-content-wrapper -->
<?php include_once('includes/footer_start.php'); ?>

    <!-- file uploads js -->
<script src="<?=base_url(); ?>assets/plugins/fileuploads/js/dropify.min.js"></script>
<script src="<?=base_url(); ?>assets/plugins/select2/js/select2.min.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $('.select').select2();
});
</script>

<?php include_once('includes/footer_end.php'); ?>
<?php
  defined('BASEPATH') OR exit('No direct script access allowed');
  include_once('includes/header_start.php'); 
?>

<?php include_once('includes/header_end.php'); 
  $sys_title = $this->user_mo->get_user();
  $id = $this->uri->segment(3);
  $data = $this->user_mo->get_patient($id);
  $other = json_decode($data[0]['other'], true);
?>
  
  <div class="wrapper">
    <div class="container">
      <!-- Page-Title -->
      <div class="row">
        <div class="col-sm-12">
          <div class="page-title-box">
            <div class="btn-group pull-right">
              <ol class="breadcrumb hide-phone p-0 m-0">
                <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>"><?php echo $sys_title[0]['title']; ?></a></li>
                <li class="breadcrumb-item active">Ubah Informasi Pasien</li>
              </ol>
            </div>
            <h4 class="page-title">Ubah Informasi Pasien</h4>
          </div>
        </div>
      </div>
      <!-- end page title end breadcrumb -->
    </div> <!-- End Container -->
  </div><!-- End Wrapper -->
  <?php
    if(!$data) {
      echo "<center><h3>Data Tidak Ditemukan,Silahkan Coba Kembali!</h3></center>";
    } else {
      $title = 'Hanya Masukkan Nilai (hanya 3 max angka)';
      $name = 'Masukkan Nama Lengkap Pasien (Hanya Boleh Huruf Saja)';
      $age = 'Masukkan Usia Pasien (Hanya Angka Max 3 Digit)';
      $type = 'Masukkan Jenis Hewan(Hanya Boleh Huruf Saja)';
      $owner = 'Masukkan Nama Lengkap Pemilik';
      $phone = '+6251256767 (Hanya Angka Min 5 Digit Max 20 Digit)';
      $height = (TYPE == 'vetbiz') ? 'Nomor Rekam Medis (Hanya Angka)' : 'Masukkan Tinggi Badan (Cm) (Hanya Angka)';
      $weight = 'Masukkan Berat Badan (Kg) (Hanya Angka)';
      $b_pressure = (TYPE == 'vetbiz') ? 'Masukkan ID Microchip (Hanya Angka)' : 'Masukkan Tekanan Darah Sistole dan Diastole (Hanya Angka)';
      $pulse = (TYPE == 'vetbiz') ? 'Masukkan Suhu Tubuh (C) (Hanya Angka)' : 'Masukkan Denyut Nadi (Kali/Menit) (Hanya Angka)';
      $respiration = (TYPE == 'vetbiz') ? 'Frekuensi Pernafasan Per Detik (Hanya Angka)' : 'Masukkan Tes Respirasi: FEV1; FVC; Rasio FVC/FEV1 Per Detik (Hanya Angka)';
      $allergy = (TYPE == 'vetbiz') ? 'Masukkan Anamnesa' : 'Masukkan Gejala Alergi & Penyakit Saat Ini';
      $diet = (TYPE == 'vetbiz') ? 'Hasil Pengamatan Tingkah Laku' : 'Masukkan Informasi Program Diet';
  ?>
  <!-- ==================
     PAGE CONTENT START
    ================== -->
  <div class="page-content-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="m-b-20">
            <a href="<?php echo base_url('user/patient_profile').'/'.$id; ?>"><button type="button" class="btn btn-primary waves-effect waves-light"><i class="fa fa-arrow-left"></i>&nbsp; Kembali Ke Profil Pasien</button></a>
          </div>
        </div>
      </div><!-- Ends Row -->
      <div class="row">
        <div class="col-12">
          <div class="card m-b-20">
            <div class="card-block">
              <blockquote class="bg-info text-white">Informasi Umum</blockquote>
              <form name="addpatient" id="addpatient" method="post" action="<?php echo base_url('user_operation/editpatient'); ?>">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama Pasien</label>
                      <input type="hidden" name="patient_id" value="<?php echo $data[0]['patient_id']; ?>">
                      <input type="text" class="form-control" name="name" required="" pattern="[A-Za-z. ]{1,}" placeholder="<?php echo $name; ?>" title="<?php echo $name; ?>" value="<?php echo $data[0]['p_name'] ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Usia Pasien</label>
                      <input type="text" class="form-control" name="age" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $age; ?>" title="<?php echo $age; ?>" value="<?php echo $data[0]['age'] ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Jenis Kelamin</label>
                      <select class="form-control" name="gender">
                      <?php foreach ($gender as $key => $val) { ?>
                          <option  value="<?php echo $key; ?>" <?php echo ($key == 0) ? 'disabled="disabled"' : '' ?> <?php echo ($data[0]['gender'] == $key) ? 'selected="selected"' : '' ; ?> ><?php echo $val; ?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>
                <?php if (TYPE == 'vetbiz') { ?>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Jenis Hewan</label>
                      <input type="text" class="form-control" name="type" required="" pattern="[A-Za-z. ]{1,}" placeholder="<?php echo $type; ?>" title="<?php echo $type; ?>" value="<?php echo $data[0]['type'] ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nama Pemilik</label>
                      <input type="text" class="form-control" name="owner" required="" pattern="[A-Za-z. ]{1,}" placeholder="<?php echo $owner; ?>" title="<?php echo $owner; ?>" value="<?php echo $data[0]['owner'] ?>">
                    </div>
                  </div>
                <?php } ?>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Nomor Telepon</label>
                      <input type="text" class="form-control" name="phone" required="" pattern="[\+0-9\-]{5,20}" placeholder="<?php echo $phone; ?>" title="<?php echo $phone; ?>" value="<?php echo $data[0]['phone'] ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label>Alamat</label>
                      <textarea name="add" rows="3" class="form-control" placeholder="Masukkan Alamat Sekarang" required=""><?php echo $data[0]['add'] ?></textarea>
                    </div>
                  </div>
                </div>
                <blockquote class="bg-info text-white mt-5">Informasi Medis</blockquote>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Nomor Rekam Medis' : 'Tinggi'; ?></label>
                      <input type="text" class="form-control" name="height" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $height; ?>" title="<?php echo $height; ?>" value="<?php echo $data[0]['height'] ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Berat Badan</label>
                      <input type="text" class="form-control" name="weight" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $weight; ?>" title="<?php echo $weight; ?>" value="<?php echo $data[0]['weight'] ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Golongan Darah</label>
                      <select class="form-control" name="b_group" title="Pilih Hasil Pemeriksaan Golongan Darah" required="">
                      <?php foreach ($blood as $key => $val) { ?>
                        <option value="<?php echo $val ?>" <?php echo ($key == 0) ? 'disabled="disabled"' : '' ?> <?php echo ($val == $data[0]['b_group']) ? 'selected="selected"' : '' ?>><?php echo $val; ?></option>
                      <?php } ?>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'ID Microchip' : 'Tekanan Darah'; ?></label>
                      <input type="text" class="form-control" name="b_pressure" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $b_pressure; ?>" title="<?php echo $b_pressure; ?>" value="<?php echo $data[0]['b_pressure'] ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Suhu Tubuh' : 'Denyut Nadi'; ?></label>
                      <input type="text" class="form-control" name="pulse" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $pulse; ?>" title="<?php echo $pulse; ?>" value="<?php echo $data[0]['pulse'] ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Frekuensi Pernafasan' : 'Respirasi'; ?></label>
                      <input type="text" class="form-control" name="respiration" required="" pattern="[0-9]+([\.,][0-9]+)?" placeholder="<?php echo $respiration; ?>" title="<?php echo $respiration; ?>" value="<?php echo $data[0]['respiration'] ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Anamnesa' : 'Alergi & Penyakit Saat Ini'; ?></label>
                      <input type="text" class="form-control" name="allergy" required="" placeholder="<?php echo $allergy; ?>" title="<?php echo $allergy; ?>" value="<?php echo $data[0]['allergy'] ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label><?php echo (TYPE == 'vetbiz') ? 'Tingkah Laku' : 'Diet'; ?></label>
                      <input type="text" class="form-control" name="diet" required="" placeholder="<?php echo $diet; ?>" title="<?php echo $diet; ?>" value="<?php echo $data[0]['diet'] ?>">
                    </div>
                  </div>
                </div>
                <div id="medinfo" rel="<?php echo (empty($other)) ? '' : count($other); ?>">
                <?php if (!empty($other)) {
                  foreach ($other as $key => $val){ ?>
                  <div class="row medinfo<?php echo $key; ?>">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-4">
                            <input type="text" class="form-control" name="other[<?php echo $key; ?>][label]" value="<?php echo $val['label']; ?>" placeholder="Masukkan Label" required="">
                          </div>
                          <div class="col-md-6">
                            <input type="text" class="form-control" name="other[<?php echo $key; ?>][value]" value="<?php echo $val['value']; ?>" placeholder="Masukkan Informasi" required="">
                          </div>
                          <div class="col-md-2">
                            <button type="button" onclick="remform('medinfo<?php echo $key; ?>');" class="btn btn-danger waves-effect waves-light pull-right">
                              <i class="fa fa-close"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2"></div>
                  </div>
                <?php }
                } ?>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><i class="fa fa-save"></i>&nbsp; Simpan Detil Pasien</button>
                  </div>
                  <div class="col-md-6">
                    <button type="button" onclick="addform('medinfo');" class="btn btn-info waves-effect waves-light pull-right">Tambahkan Info</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div><!-- Ends Row -->
    </div><!-- Ends container -->
  </div><!-- Ends page-content-wrapper -->
  <div class="hidden-xs-up" id="html"></div>
<?php } include_once('includes/footer_start.php'); ?>

<script type="text/javascript">
function addform(id){
  var rel = parseInt($('#' + id).attr('rel'));
  var html = '<div class="row medinfo' + (rel + 1) + '"><div class="col-md-2"></div><div class="col-md-8"><div class="form-group"><div class="row"><div class="col-md-4"><input type="text" class="form-control" name="other[' + rel + '][label]" placeholder="Masukkan Label" required=""></div><div class="col-md-6"><input type="text" class="form-control" name="other[' + rel + '][value]" placeholder="Masukkan Informasi" required=""></div><div class="col-md-2"><button type="button" onclick="remform(' + "'medinfo" + (rel + 1) + "'" + ');" class="btn btn-danger waves-effect waves-light pull-right"><i class="fa fa-close"></i></button></div></div></div></div><div class="col-md-2"></div></div>';
  $('#' + id).attr('rel', rel + 1);
  $('#' + id).append(html);
}

function remform(id){
  $('.' + id).remove();
}
</script>

<?php include_once('includes/footer_end.php'); ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_mo extends CI_Model {

	public function setup($data) {
		$this->db->insert('users',$data);
		return $this->db->insert_id();
	}

  public function login_data($user_name,$user_password) {
		$res = $this->db->get_where('users',array('user_name'=>$user_name,'password'=>$user_password))->row_array();
		return (count($res)>=1) ? $res : false ;
	}

	function read($table, $condition = null, $fields = "*"){
		return $this->db->query("SELECT $fields FROM $table $condition")->result_array();
	}

	function one($table, $condition = null, $fields = "*"){
		return $this->db->query("SELECT $fields FROM $table $condition")->row_array();
	}

	public function getuser() {
		$id = $this->session->userdata('userid');
		$res = $this->db->select('*')
						->from('users')
						->where('user_id',$id)
						->get()->result_array();
		return $res[0];
	}

	function img(){
		$res = $this->db->select('title, logo')
						->from('users')
						->where('user_id','1')
						->get()->result_array();
		return $res;
	}

	public function get_user() {
		$id = $this->session->userdata('userid');
		$res = $this->db->select('*')
						->from('users')
						->where('user_id',$id)
						->get()->result_array();
		return $res;
	}

	public function getemail($email) {
		$res = $this->db->select('email,password')
						->from('users')
						->where('email',$email)
						->get()->result_array();
		return $res;
	}
	
	public function patient_info() {
		$id = $this->session->userdata('userid');
		$this->db->select('*');
		$this->db->from('patient');
		$this->db->where('user_id',$id);
		$data = $this->db->get();
		return $data->result_array();
	}

	public function addpatient($data) {
		$data['user_id'] = $this->session->userdata('userid');
		$data['date'] = date('Y-m-d');
		$this->db->insert('patient',$data);
		return true;
	}

	public function get_patient($id) {
		$uid = $this->session->userdata('userid');
		$data = $this->db->query('SELECT * FROM patient WHERE user_id = ' . $uid . ' AND patient_id = '.$id);
		return $data->result_array();
	}
	
	public function editpatient($data) {
		$this->db->set('p_name',$data['p_name']);
		$this->db->set('age',$data['age']);
		$this->db->set('gender',$data['gender']);
		$this->db->set('type',$data['type']);
		$this->db->set('owner',$data['owner']);
		$this->db->set('phone',$data['phone']);
		$this->db->set('add',$data['add']);
		$this->db->set('height',$data['height']);
		$this->db->set('weight',$data['weight']);
		$this->db->set('b_group',$data['b_group']);
		$this->db->set('b_pressure',$data['b_pressure']);
		$this->db->set('pulse',$data['pulse']);
		$this->db->set('respiration',$data['respiration']);
		$this->db->set('allergy',$data['allergy']);
		$this->db->set('diet',$data['diet']);
		$this->db->set('other',$data['other']);
		$this->db->where('patient_id',$data['patient_id']);
		$this->db->update('patient');
		return true;
	}

	public function updatelogo($data, $type = 'logo') {
		$this->db->set($type,$data);
		$this->db->where('user_id',$this->session->userdata('userid'));
		$this->db->update('users');
		return true;
	}

	public function deletepatient($id) {
		$uid = $this->session->userdata('userid');
		$this->db->where('patient_id', $id);
		$this->db->where('user_id', $uid);
		$this->db->delete('patient');
		return true;
	}

	public function chkappointment($data) {
		$id = $this->session->userdata('userid');
		$res = $this->db->query("SELECT patient_id FROM appointment WHERE patient_id=".$data['patient_id']." AND user_id='$id' AND date='".$data['date']."'");
		if(count($res->result_array())==1) {
			return 1;
		} else {
			$res1 = $this->db->query("SELECT patient_id FROM appointment WHERE time='".$data['time']."' AND user_id='$id' AND date='".$data['date']."'");
			if(count($res1->result_array())==1) {
				return 2;
			} else {
				$data['user_id'] = $id;
				$this->db->insert('appointment',$data);
			}
		}
		
	}

	public function get_appointment($ids) {
		$id = $this->session->userdata('userid');
		$data = $this->db->query('SELECT * FROM appointment WHERE user_id = '. $id .' AND patient_id = '.$ids);
		return $data->result_array();
	}

	public function get_prescription($ids) {
		$id = $this->session->userdata('userid');
		$data = $this->db->select('*')
					->from('prescription')
					->where('user_id',$id)
					->where('patient_id',$ids)
					->get()->result_array();
		return $data;
	}

	public function addprescription($data) {
		$data['user_id'] = $this->session->userdata('userid');
		$this->db->insert('prescription',$data);
		return true;
	}

	public function getprescription() {
		$id = $this->session->userdata('userid');
		$res = $this->db->query('SELECT prescription_id, p_name, DATE_FORMAT(prescription.date, "%M %d,%Y") as date FROM patient LEFT JOIN prescription ON patient.patient_id = prescription.patient_id WHERE prescription.user_id = ' . $id);
		return $res->result_array();
	}

	public function getprescriptionbyid($ids) {
		$id = $this->session->userdata('userid');
		$res = $this->db->select('*')
					->from('prescription')
          ->join('patient','patient.patient_id = prescription.patient_id','left')
					->where('prescription_id',$ids)
					->where('prescription.user_id',$id)
					->get()
					->result_array();
		return $res;
	}

	public function getp_name($ids) {
		$id = $this->session->userdata('userid');
		$res = $this->db->select('*')
					->from('patient')
					->where('patient_id',$ids)
					->where('user_id',$id)
					->get()
					->result_array();
		return $res[0];

	}

	public function deleteprescription($id) {
		$this->db->where('prescription_id', $id);
		$this->db->delete('prescription');
		return true;
	}

	public function createinvoice($data) {
		$data['user_id'] = $this->session->userdata('userid');
		$this->db->insert('invoice',$data);
		return true;
	}

	public function getinvoice() {
		$id = $this->session->userdata('userid');
		$res = $this->db->query('SELECT invoice_id, p_name, payment_status, invoice_amount, DATE_FORMAT(invoice.invoice_date, "%M %d,%Y") as date FROM patient LEFT JOIN invoice ON patient.patient_id = invoice.patient_id WHERE invoice.user_id = ' . $id);
		return $res->result_array();
	}

	public function getinvoicebyid($ids) {
		$id = $this->session->userdata('userid');
		$res = $this->db->select('*')
					->from('invoice')
					->where('invoice_id',$ids)
					->where('user_id',$id)
					->get()
					->result_array();
		return $res;
	}

	public function deleteinvoice($id) {
		$this->db->where('invoice_id', $id);
		$this->db->delete('invoice');
		return true;
	}

	public function get_invoice($ids) {
		$id = $this->session->userdata('userid');
		$data = $this->db->select('*')
					->from('invoice')
					->where('patient_id',$ids)
					->where('user_id',$id)
					->get()->result_array();
		return $data;
	}

	public function updateprofile($data) {
		$this->db->set('doctor_name',$data['name']);
		$this->db->set('title',$data['title']);
		$this->db->set('email',$data['email']);
		$this->db->set('mobile',$data['phone']);
		$this->db->where('user_name',$data['user_name']);
		$this->db->update('users');
		return true;
	}

	public function updateusers($user_id, $data) {
		$id['user_id'] = $user_id;
    $this->db->update('users',$data, $id);
		return true;
	}

	public function user_info() {
		return $this->db->select('*')
			->from('users')
			->where('user_id',$this->session->userdata('userid'))
			->get()->row_array();
	}

	public function updatepassword($data) {
		$id = $this->session->userdata('userid');
		$pwd = $this->db->query("SELECT user_id FROM users WHERE user_id = $id AND password ='".$data['cpass']."'");
		if(count($pwd->result_array()) == 1) {
			$this->db->query("UPDATE users SET password='".$data['password']."' WHERE user_id = $id AND user_name='".$data['user_name']."'");
			return true;
		} else {
			return false;
		}
	}

	public function total_count() {
		$id = $this->session->userdata('userid');
    $res = $this->db->select('*')
                ->from('appointment')
                ->where('user_id',$id)
                ->get()->result_array();
    $res1 = $this->db->select('*')
                 ->from('patient')
                 ->where('user_id',$id)
                 ->get()->result_array();
    $res2 = $this->db->select('*')
								 ->from('invoice')
								 ->where('user_id',$id)
								 ->get()->result_array();
		$res3 = $this->db->select('*')
								 ->from('invoice')
								 ->where('user_id',$id)
								 ->where('payment_status','Paid')
								 ->get()->result_array();
		$total = 0;
		for($i=0; $i< count($res3); $i++) {
			$amount = json_decode($res3[$i]['invoice_amount']);
			$total += array_sum($amount);
		}
		
    return array('appointment'=>count($res),'patient'=>count($res1),'invoice'=>count($res2),'total'=>$total);
	}
	
	public function appointment_list() {
		$id = $this->session->userdata('userid');
		$res = $this->db->query("SELECT appointment.*, patient.p_name FROM appointment LEFT JOIN patient ON patient.patient_id = appointment.patient_id WHERE appointment.user_id = $id AND appointment.date = DATE_FORMAT(CURDATE(),'%Y/%m/%d')");
		return $res->result_array();
	}

	public function appointmentlist($date) {
		$id = $this->session->userdata('userid');
		$res = $this->db->select('p_name,appointment.time')
						->from('appointment')
						->join('patient','appointment.patient_id=patient.patient_id')
						->where('appointment.date',$date)
						->where('appointment.user_id',$id)
						->get();
		return $res->result_array();
	}

	function patientchart() {
		$bef = (date('Y') - 1) . '-' . (date('m') - 1) . '-' . 1;
		$aft = date('Y-m-d');
		$id = $this->session->userdata('userid');
		return $this->db->query("SELECT MONTHNAME(patient.date) AS month, Month(patient.date) as num_month, count(*) AS total FROM patient WHERE user_id = $id AND patient.date >= CAST('$bef' AS DATE) AND patient.date <= CAST('$aft' AS DATE) GROUP BY month ORDER BY num_month ASC")->result_array();
	}

	function prescriptionchart() {
		$bef = (date('Y') - 1) . '-' . (date('m') - 1) . '-' . 1;
		$aft = date('Y-m-d');
		$id = $this->session->userdata('userid');
		return $this->db->query("SELECT MONTHNAME(prescription.date) AS month, Month(prescription.date) as num_month, count(*) AS total FROM prescription WHERE user_id = $id AND prescription.date >= CAST('$bef' AS DATE) AND prescription.date <= CAST('$aft' AS DATE) GROUP BY month ORDER BY num_month ASC")->result_array();
	}

	public function appointmentchart() {
		$bef = (date('Y') - 1) . '-' . (date('m') - 1) . '-' . 1;
		$aft = date('Y-m-d');
		$id = $this->session->userdata('userid');
		$res = $this->db->query("SELECT MONTHNAME(appointment.date) AS month, Month(appointment.date) as num_month, count(*) AS total FROM appointment WHERE user_id = $id AND appointment.date >= CAST('$bef' AS DATE) AND appointment.date <= CAST('$aft' AS DATE) GROUP BY month ORDER BY num_month ASC");
		return $res->result_array();
	}

	public function invoicechart() {
		$id = $this->session->userdata('userid');
		$res = $this->db->query("SELECT payment_status as status, count(*) as total FROM invoice WHERE user_id = $id GROUP BY status");
		$res = $res->result_array();

		foreach ($res as $key => $val) {
			$res[$key]['status'] = ($val['status'] == 'Paid') ? 'Terbayar' : 'Belum Terbayar';
		}

		return $res;
	}

	public function patient() {
		$id = $this->session->userdata('userid');
		$data = $this->db->query("SELECT * FROM patient WHERE $id = patient.user_id ORDER BY patient_id DESC LIMIT 5");
		return $data->result_array();
	}
}
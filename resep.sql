-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 19, 2020 at 07:32 PM
-- Server version: 10.1.37-MariaDB-0+deb9u1
-- PHP Version: 7.4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `resep`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `appointment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `time` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`appointment_id`, `user_id`, `patient_id`, `date`, `time`) VALUES
(1, 1, 1, '2020-06-16', '09:30 PM'),
(2, 4, 1, '2020-06-17', '4:15 PM'),
(3, 3, 5, '2020-06-25', '5:15 PM'),
(4, 1, 7, '2020-06-24', '5:30 PM');

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `payment_mode` varchar(10) NOT NULL,
  `payment_status` varchar(10) NOT NULL,
  `invoice_title` varchar(100) NOT NULL,
  `invoice_amount` varchar(100) NOT NULL,
  `invoice_date` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `user_id`, `patient_id`, `payment_mode`, `payment_status`, `invoice_title`, `invoice_amount`, `invoice_date`) VALUES
(1, 1, 1, 'Cash', 'Paid', '[\"PS 0001 01 2020\"]', '[\"100000\"]', '2020-01-12'),
(2, 4, 3, 'Cash', 'Paid', '[\"cok\"]', '[\"10000\"]', '2020-04-17'),
(3, 3, 5, 'Cash', 'Paid', '[\"io\"]', '[\"9000\"]', '2020-04-24'),
(4, 1, 7, 'Cheque', 'Unpaid', '[\"resep\"]', '[\"75000\"]', '2020-06-08');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `patient_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `p_name` varchar(50) NOT NULL,
  `age` varchar(50) NOT NULL,
  `gender` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `add` varchar(50) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(50) NOT NULL,
  `b_group` varchar(10) NOT NULL,
  `b_pressure` varchar(50) NOT NULL,
  `pulse` varchar(50) NOT NULL,
  `respiration` varchar(50) NOT NULL,
  `allergy` varchar(50) NOT NULL,
  `diet` varchar(10) NOT NULL,
  `other` text,
  `date` date NOT NULL,
  `type` text NOT NULL,
  `owner` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`patient_id`, `user_id`, `p_name`, `age`, `gender`, `phone`, `add`, `height`, `weight`, `b_group`, `b_pressure`, `pulse`, `respiration`, `allergy`, `diet`, `other`, `date`, `type`, `owner`) VALUES
(1, 1, 'kelinci', '23', 1, '622153123667', 'Jakarta Selatan', '178', '70', 'A+', '110', '122', '111', 'Susu Sapi Perah Yang diberikan makan rumput buatan', 'Diet Prote', '{\"1\":{\"label\":\"iu\",\"value\":\"iu\"},\"2\":{\"label\":\"lho\",\"value\":\"kok\"}}', '2020-05-03', 'terwelu', 'mboh kon'),
(2, 1, 'coba', '10', 1, '+9876', 'asdi saoidj a', '130', '27', 'A+', '90', '90', '90', '90', '90', '[{\"label\":\"lho\",\"value\":\"kok\"},{\"label\":\"iso\",\"value\":\"ne\"}]', '2020-04-03', '', ''),
(3, 1, 'ok', '71', 1, '+2222', 'aksda sdaskdja', '170', '60', 'A+', '90', '90', '90', '90', '90', 'null', '2020-03-03', '', ''),
(4, 1, 'cocok', '87', 1, '098876', 'cocok', '160', '60', 'A+', '90', '90', '90', '90', '90', '[{\"label\":\"ok\",\"value\":\"ok\"}]', '2020-02-03', '', ''),
(5, 3, 'yoyo', '90', 1, '+900000', '90', '90', '90', 'A+', '90', '90', '90', '90', '90', '[{\"label\":\"yo\",\"value\":\"yo\"}]', '2020-01-03', '', ''),
(6, 1, 'oooo', '9', 2, '99999', 'okok', '10', '1', 'A+', '10', '10', '10', '10', '10', 'null', '2020-06-01', 'okok', 'okok'),
(7, 1, 'nam', '11', 2, '+62878', 'jkt BRT', '11', '2', 'A-', '11', '11', '11', '11', '11', '[{\"label\":\"11\",\"value\":\"11\"}]', '2020-06-02', '', ''),
(8, 1, 'nama', '1', 1, '1111111', '11', '1', '1', 'AB', '1', '1', '1', '1', '1', '[{\"label\":\"1\",\"value\":\"1\"},{\"label\":\"2\",\"value\":\"2\"}]', '2020-06-08', 'nama', 'nama'),
(9, 1, 'namae', '0.1', 0, '010101010', '01010skjsdkj', '0.1', '0.1', 'DEA 1.2', '0.1', '0.1', '0.1', '0.1', '0.1', '[{\"label\":\"0.1\",\"value\":\"0.1\"}]', '2020-06-14', 'hewan', 'hewan');

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

CREATE TABLE `prescription` (
  `prescription_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `symptoms` varchar(100) NOT NULL,
  `diagnosis` varchar(100) NOT NULL,
  `medicine` varchar(100) NOT NULL,
  `m_note` varchar(100) NOT NULL,
  `test` varchar(100) NOT NULL,
  `t_note` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `m_note2` text,
  `t_note2` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`prescription_id`, `user_id`, `patient_id`, `symptoms`, `diagnosis`, `medicine`, `m_note`, `test`, `t_note`, `date`, `m_note2`, `t_note2`) VALUES
(1, 1, 1, 'Batuk-batuk \r\nPilek\r\n', 'Influensa', '[\"Aricept 5mg\"]', '[\"s 1 dd ;  10 tablet\"]', '[\"Tidak ada\"]', '[\"Belum Perlu\"]', '2020-01-12', NULL, NULL),
(2, 1, 2, 'oo', 'ii', '[\"uu\",\"tt\"]', '[\"yy\",\"rr\"]', '', '', '2020-04-10', NULL, NULL),
(3, 1, 3, 'odkasd asdasd', 'sjdaskda asdasd', '', '', '[\"cobaan\"]', '[\"coba\"]', '2020-04-10', NULL, NULL),
(4, 1, 2, 'oko', 'kok', '', '', '[\"cok\",\"tol\"]', '[\"cok\",\"tol\"]', '2020-04-13', NULL, '[\"cok\",\"tol\"]'),
(5, 4, 2, 'cok', 'cok', '[\"ko\"]', '[\"ko\"]', '', '', '2020-04-17', '[\"ko\"]', NULL),
(6, 3, 5, 'io', 'io', '[\"io\"]', '[\"io\"]', '', '', '2020-04-24', '[\"io\"]', NULL),
(7, 1, 3, 'gejala', 'diagnosa', '[\"1\",\"2\"]', '[\"1\",\"2\"]', '', '', '2020-06-08', '[\"1\",\"2\"]', NULL),
(8, 1, 4, 'gejala', 'gejala', '', '', '[\"1\",\"2\"]', '[\"1\",\"2\"]', '2020-06-08', NULL, '[\"1\",\"2\"]');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `doctor_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `favicon` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `office` text NOT NULL,
  `sip` text NOT NULL,
  `specialist` text NOT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `doctor_name`, `email`, `mobile`, `password`, `logo`, `favicon`, `title`, `office`, `sip`, `specialist`, `status`) VALUES
(1, 'admin', 'Promedis', 'aleppo.coy@gmail.com', '622153123667', 'YWRtaW4=', 'logo_promedis_new600x143.png', 'ic_promedis1.png', 'Promedis Resep', '[{\"address\":\"oi\",\"day\":\"oi\",\"hour\":\"oi\"},{\"address\":\"yu\",\"day\":\"yu\",\"hour\":\"yu\"}]', 'oi', 'oi', 2),
(2, 'promedi', 'coba', 'coba@co.ba', '0099', 'UGFzc21sZzAxMDE=', '', '', '', '', '', '', 0),
(3, 'mboh', 'mboh', 'mboh@mboh.com', '0987', 'bWJvaA==', 'male_avatar.png', 'avatar-1.jpg', 'mboh', '[{\"address\":\"mboh\",\"day\":\"mboh\",\"hour\":\"mboh\"}]', 'mboh', 'mboh', 1),
(4, 'cobaan', 'mboh', 'mboh@mb.oh', '098765', 'Y29iYWFu', 'logo_promedis_new600x143.png', 'ic_promedis1.png', '', '', '', '', 0),
(5, 'raruh', 'raruh', 'raruh@raruh.com', '0987', 'bWJvaA==', '', '', '', '[{\"address\":\"okok\",\"day\":\"okokok\",\"hour\":\"okoko\"},{\"address\":\"kok\",\"day\":\"okokok\",\"hour\":\"okoko\"}]', 'okokok', 'kokok', 0),
(6, 'coba', '', 'email@email.co', '', 'Y29iYQ==', '', '', '', '', '', '', 0),
(7, 'nama', '', 'email@email.com', '', 'bmFtYQ==', '', '', '', '', '', '', 0),
(8, 'dktr', '', 'aleppo.coy@gmail.co', '', 'ZGt0cg==', '', '', '', '', '', '', 1),
(9, 'okbos', '', 'aleppo_coy@yahoo.co.id', '', 'b2tib3M=', '', '', '', '', '', '', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`appointment_id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`invoice_id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `prescription`
--
ALTER TABLE `prescription`
  ADD PRIMARY KEY (`prescription_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `invoice_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `prescription`
--
ALTER TABLE `prescription`
  MODIFY `prescription_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
